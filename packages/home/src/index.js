import { registerApplication, start } from 'single-spa';
// import {
//   constructApplications,
//   constructLayoutEngine,
//   constructRoutes,
// } from "single-spa-layout";
// import layout from '../public/index.html';

registerApplication(
  'product-image',
  () => import('productImage/ProductImage'),
  location => location.pathname.startsWith('/')
);

registerApplication(
  'header',
  () => import('nav/Header'),
  location => location.pathname.startsWith('/'),
  {custom: 'IG SASS', title: 'React JS'}
);

registerApplication(
  'footer',
  () => import('nav/Footer'),
  location => location.pathname.startsWith('/')
);

registerApplication(
  'buy-tools',
  () => import('buyTools/BuyTools'),
  location => location.pathname.startsWith('/')
);

registerApplication(
  'sliderVue',
  () => import('sliderVue/Slider'),
  location => location.pathname.startsWith('/'),
  {heading: 'Vue Carousel'}
);

registerApplication(
  'post-content',
  () => import('sliderVue/PostContent'),
  location => location.pathname.startsWith('/'),
);

// const routes = constructRoutes(layout);
// const applications = constructApplications({
//   routes,
//   loadApp({ name }) {
//     return System.import(name);
//   },
// });
// const layoutEngine = constructLayoutEngine({ routes, applications });

// applications.forEach(registerApplication);
// layoutEngine.activate();

start();
