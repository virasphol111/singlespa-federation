import fruit from 'home/fruit';
import singleSpaHtml from 'single-spa-html';
import store from 'store/store';

const template = `
  <div class="w-full bg-white border border-gray-200 rounded-lg shadow">
    <div class="border-b flex justify-center items-center border-gray-200 rounded-t-lg bg-gray-50 dark:border-gray-700 dark:text-gray-400 dark:bg-gray-800">
      <img src="https://www.cnet.com/a/img/resize/39e05dbff495f3ecbf044772f45baf993b92890a/hub/2011/01/18/6ee1f979-f0f7-11e2-8c7c-d4ae52e62bcc/HTML5_Logo_550px.png?auto=webp&fit=crop&height=900&width=1200" class="h-8 mr-3" />
      <h2 class="text-xl p-4">HTML Template</h2>
    </div>
    <div
      class="product-images"
      style="display: grid; grid-template-columns: repeat(5, 20%);"
    >
    </div>
  </div>
`;

const jsComponent = singleSpaHtml({
  template,
});

jsComponent.originalMount = jsComponent.mount;
jsComponent.mount = function(opts, props) {
  return jsComponent.originalMount(opts, props)
    .then(() => {
      const el = document.querySelector('.product-images');
      const html = 
        fruit
          .map(({image}, index) => `
            <img src="${image}" style="max-width: 100%" data-index="${index}" />
          `)
          .join('');
      el.innerHTML = html;
      document
        .querySelectorAll('.product-images img')
        .forEach(el => el.addEventListener('click', (evt) => {
          store.image = parseInt(
            evt.target.getAttribute('data-index')
          );
        }))
    });
};

export const bootstrap = jsComponent.bootstrap;
export const mount = jsComponent.mount;
export const unmount = jsComponent.unmount;
