import { h, createApp } from 'vue';
import singleSpaVue from 'single-spa-vue';

import PostContent from './PostContent.vue';

const vueLifecycles = singleSpaVue({
  createApp,
  appOptions: {
    render() {
      return h(PostContent, { 
        mountParcel: this.mountParcel,
      });
    },
  },
});

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;
