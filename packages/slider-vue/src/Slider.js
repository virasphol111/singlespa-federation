import { h, createApp } from 'vue';
import singleSpaVue from 'single-spa-vue';

import Slider from './HeroSlider.vue';
import router from './router';

const vueLifecycles = singleSpaVue({
  createApp,
  appOptions: {
    render() {
      return h(Slider, { 
        mountParcel: this.mountParcel,
        name: this.name,
        heading: this.heading,
      });
    },
  },
  handleInstance: (app) => {
    app.use(router);
  }
});

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;
