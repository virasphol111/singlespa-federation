import HeroSlider from "./HeroSlider.vue";
import PostContent from "./PostContent.vue";
import {createRouter, createWebHashHistory} from 'vue-router';

const routes = [
  { path: '/', component: HeroSlider },
  { path: '/post-content', component: PostContent },
]


const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;