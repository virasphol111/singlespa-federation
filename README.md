# Getting Started

- To be install all framework dependency : just run this command.
- Or go to each project and run it one by one

```sh
yarn install
yarn start
```
===============
- Home : 'http://localhost:3001/'
- Product Image : 'http://localhost:3002/'
- React(Navbar, Footer) : 'http://localhost:3003/'
- Buy Tool : 'http://localhost:3004/'
- Store : 'http://localhost:3005/'
- Slider Vue : 'http://localhost:3006/'
================

- Change app endpoint we need : Go to 
  webpack.config.js 

  ```
  output: {
    publicPath: 'http://localhost:3004/'
  },
  ```

  and rerun command again.

Open [http://localhost:3001](http://localhost:3001).
